import pandas as pd
import geopandas as gpd
import numpy as np

# This script takes a text file of Albers tiles and samples them then indexes the Australian 

vectorfile = 'Australian_Albers_Polygons.shp'
tiles = 'au_tiles_land.txt'

# Import text file of Australian ALbers tile codes
au_tiles_land = pd.read_csv(tiles, header=None)
sample = au_tiles_land.sample(frac=0.2)
# write out sampled tiles in format the queue can consume
np.savetxt('sample.txt', sample.values, fmt = "%s")
# regex to ensure match with albers shapefile label
sample = sample.replace({' ': ','}, regex=True)

# Import vector file of Australian Albers polygon
albers_polygons = gpd.read_file(vectorfile)

# Index with sampled tiles
sample_albers_polygons = albers_polygons[albers_polygons['label'].isin(sample[0])]

# write out vector file
sample_albers_polygons.to_file('sample.shp')
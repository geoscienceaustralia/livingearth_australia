#!/usr/bin/env python
"""
A script to convert a multiband level4 image into a single band image.

The value of each band is defined in a CSV file.

For the script to work it assumes that the band names are the same as the level4 names used within the classification.

Dan Clewley
2020-05-19

"""
import argparse
import os
import sys

import numpy
import pandas
import rasterio
import xarray

from le_lccs.le_classification import l4_base

CLASS_CODE_IDS = os.path.join(os.path.dirname(__file__), "australia_layers_cols_sorted.csv")

def get_level4_pixel_id(classification_array, class_codes_csv=CLASS_CODE_IDS):
    """"
    Gets a unique id for each level4 class based on an input CSV file

    :param xarray.Dataset classification_array: xarray dataset containing classified level 4 data layers.
    :param str colour_scheme_csv: The name of the colour scheme in LCCS_L4_COLOURS_CSV_DICT if using a
                                  standard colour scheme or the path to a CSV file for a non-standard
                                  scheme.

    """
    # Create a list of non-layer names in CSV.
    # For all other columns the heading is assumed to be the layer name
    non_layer_names = ["pixelID","LCCS_Code", "LCCS_Description", "Red", "Green", "Blue", "Alpha"]

    colour_scheme = pandas.read_csv(class_codes_csv)

    # Set up arrays for output
    pixel_id = numpy.zeros_like(classification_array[l4_base.LEVEL3_LAYER_NAME].values,
                                dtype=numpy.uint8)

    # Itterate through rows colour scheme
    for _, line in colour_scheme.iterrows():

        # If the column is a layer name add values to
        # 'layer_val_list'
        layer_val_list = []
        for layer, val in line.iteritems():
            if layer not in non_layer_names:
                layer_val_list.append((layer, val))

        # Using 'layer_val_list' get subset mask of elements (pixels) which
        # should be coloured
        subset = l4_base.get_colours_mask(classification_array, layer_val_list)

        # Colour output arrays
        pixel_id[subset] = line["pixelID"]

    return pixel_id

def multi_to_single_band(multi_band_input, single_band_output, class_codes_csv=CLASS_CODE_IDS):
    """
    A function to calculate simple change between two geotiffs.
    """

    print("Reading in data...")
    # Open files as a data array. Assume want entire image not a subset.
    # Specify chunks to read using dask, won't place the whole thing in memory
    # to start with.
    l4_xr = xarray.open_rasterio(multi_band_input, chunks={'band': 1, 'x': 1024, 'y': 1024})

    # Convet to a dataframe with each band as a separate variable.
    # Take variable names from band description
    level4_list = []

    for i, layer_name in enumerate(l4_xr.attrs["descriptions"]):
        level4_list.append(l4_xr[i].to_dataset(name=layer_name).drop_vars("band"))

    # Merge into a single xarray
    classification_data = xarray.merge(level4_list)

    print("Getting pixel IDs...")
    pixel_id = get_level4_pixel_id(classification_data, class_codes_csv=class_codes_csv)

    print("Writing out...")
    # Open file in rasterio so we can copy CRS and transform across to output file
    in_ds = rasterio.open(multi_band_input, "r")

    # Save out, set driver options based on output type.
    if os.path.splitext(single_band_output)[-1] == ".img":
        out_ds = rasterio.open(single_band_output, "w", driver="HFA",
                           height=classification_data.dims["y"], width=classification_data.dims["x"],
                           count=1, dtype=numpy.uint8, nodata=0,
                           crs=in_ds.crs, transform=in_ds.transform,
                           compressed="YES")
    elif os.path.splitext(single_band_output)[-1] == ".kea":
        out_ds = rasterio.open(single_band_output, "w", driver="KEA",
                           height=classification_data.dims["y"], width=classification_data.dims["x"],
                           count=1, dtype=numpy.uint8, nodata=0,
                           crs=in_ds.crs, transform=in_ds.transform)
    elif os.path.splitext(single_band_output)[-1] == ".tif":
        additional_creation_options = {"compress" : "LZW",
                                       "tiled" : "True"}
        out_ds = rasterio.open(single_band_output, "w", driver="GTiff",
                           height=classification_data.dims["y"], width=classification_data.dims["x"],
                           count=1, dtype=numpy.uint8, nodata=0,
                           crs=in_ds.crs, transform=in_ds.transform, **additional_creation_options)
    out_ds.write(pixel_id, 1)
    out_ds.set_band_description(1, "ID")

    in_ds.close()
    out_ds.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("inputimage", type=str,
                        help="Input 4 band L4 classification image")
    parser.add_argument("outputimage", type=str,
                        help="Output single band image (should be .img format so RAT can be added.)")
    parser.add_argument("--pixel_ids", type=str, default=CLASS_CODE_IDS,
                        help="CSV with pixel IDs for each Level 4 class. Default {}".format(CLASS_CODE_IDS))
    args = parser.parse_args()
    
    if os.path.splitext(args.outputimage)[-1] not in [".img", ".kea", ".tif"]:
        print("Output must be *.img (ERDAS Imagine format), *.kea (KEA format) "
              "or *.tif (GeoTiff format).", file=sys.stderr)
        sys.exit(1)

    # Convert to a single band image
    multi_to_single_band(args.inputimage, args.outputimage, args.pixel_ids)


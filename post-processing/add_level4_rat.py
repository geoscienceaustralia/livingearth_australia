#!/usr/bin/env python

"""
Add RAT to Level 4 single band image

Dan Clewley
2020-05-19

"""
import argparse
import os

import numpy
import pandas

import gdal
from rios import ratapplier
from rsgislib import rastergis

CLASS_CODE_IDS = os.path.join(os.path.dirname(__file__), "australia_layers_cols_sorted.csv")

def set_thematic(classification):
    ds = gdal.Open(classification, gdal.GA_Update)

    band = ds.GetRasterBand(1)
    band.SetMetadataItem('LAYER_TYPE', 'thematic')

    ds = None

def _apply_add_class_name_rios(info, inputs, outputs, otherargs):
    """
    Apply class name and colours to RAT.
    """

    class_id = inputs.inrat.Class.astype(numpy.uint8)
    class_code = numpy.empty_like(class_id, dtype=numpy.dtype("U50"))
    class_code_description = numpy.empty_like(class_id, dtype=numpy.dtype("U200"))
    red = inputs.inrat.Red
    green = inputs.inrat.Green
    blue = inputs.inrat.Blue
    alpha = numpy.empty_like(red)
    
    colour_scheme = pandas.read_csv(otherargs.class_codes_csv)

    red[...] = 0
    green[...] = 0
    blue[...] = 0
    alpha[...] = 255
    class_code[...] = ""
    class_code_description[...] = ""

    for _, line in colour_scheme.iterrows():
        # Get Index
        i = line["pixelID"]
        class_code[i] = line["LCCS_Code"]
        class_code_description[i] = "{}: {}".format(line["LCCS_Code"], line["LCCS_Description"])
        red[i] = int(line["Red"])
        green[i] = int(line["Green"])
        blue[i] = int(line["Blue"])
        alpha[i] = int(line["Alpha"])

    outputs.outrat.LCCS_Code = class_code
    outputs.outrat.LCCS_Description = class_code_description
    outputs.outrat.Red = red
    outputs.outrat.Green = green
    outputs.outrat.Blue = blue
    outputs.outrat.Alpha = alpha

def add_classification_rat(classification, class_codes_csv):
    """
    Add class name for each integer class.
    Uses RIOS ratapplier
    """
   
    in_rats = ratapplier.RatAssociations()
    out_rats = ratapplier.RatAssociations()
                
    in_rats.inrat = ratapplier.RatHandle(classification)
    out_rats.outrat = ratapplier.RatHandle(classification)

    otherargs = ratapplier.OtherArguments()
    otherargs.class_codes_csv = class_codes_csv

    ratapplier.apply(_apply_add_class_name_rios, in_rats, out_rats, otherargs)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Add colour scheme and classification codes to Level 4 single band image")
    parser.add_argument("classification", type=str, help="Input Classification")
    parser.add_argument("--pixel_ids", type=str, default=CLASS_CODE_IDS,
                        help="CSV with pixel IDs for each Level 4 class")
    args = parser.parse_args()


    print('Adding colours to: {}'.format(args.classification))
    set_thematic(args.classification)
    # Add histogram (automatically adds attribute table)
    rastergis.populateStats(args.classification, addclrtab=True,
                            calcpyramids=False, ignorezero=True)

    # Add pixel values to attribute table
    bandStats = []
    bandStats.append(rastergis.BandAttStats(band=1, maxField='Class'))
    rastergis.populateRATWithStats(args.classification,
                                   args.classification, bandStats)

    # Add description and colours
    add_classification_rat(args.classification, args.pixel_ids)
    
    # Add pyramids (for fast display)
    rastergis.populateStats(args.classification, addclrtab=False,
                            calcpyramids=True, ignorezero=True)
    
 

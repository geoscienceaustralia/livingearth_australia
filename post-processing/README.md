# Post Processing #

## Creating Mosaics ##

Clip to coastline using. This will clip only tiles by the coast and symlink the inland ones.
```
clip_to_coastline.py
```

Create VRTs. If using version clipped to coastline change `.tif` to `*.tif` to get clipped coastal tiles.
```
gdalbuildvrt -o lccs_2010_L4_v-1.0.0.vrt  2010/*L4_v-1.0.0.tif
gdalbuildvrt -o lccs_2010_L4_rgb_v-1.0.0.vrt  2010/*L4_rgb_v-1.0.0.tif
gdalbuildvrt -o lccs_2010_L3_rgb_v-1.0.0.vrt  2010/*L3_rgb_v-1.0.0.tif
```

Convert to GeoTiff

```
gdal_translate -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" -co "BIGTIFF=YES" lccs_2010_L4_v-1.0.0.vrt lccs_2010_L4_v-1.0.0.tif
gdal_translate -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" -co "BIGTIFF=YES" lccs_2010_L4_rgb_v-1.0.0.vrt lccs_2010_L4_rgb_v-1.0.0.tif
gdal_translate -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" -co "BIGTIFF=YES" lccs_2010_L3_rgb_v-1.0.0.vrt lccs_2010_L3_rgb_v-1.0.0.tif
```

Set band names
```
setbandname.py -f band_names.csv -i lccs_2010_L4_v-1.0.0.tif
```

Calculate stats and overviews (using RIOS)
```
gdalcalcstats --ignore 0 lccs_2010_L4_rgb_v-1.0.0.vrt
gdalcalcstats --ignore 0 lccs_2010_L3_rgb_v-1.0.0.vrt
```



## Resample to 250 m ##

```
gdalwarp -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" \
	-tr 250 -250 -te -2189542.2514926404692233 -4964936.3053171560168266 2468707.7485073595307767 -1047686.3053171562496573 \
	-t_srs EPSG:3577 -r mode -multi lccs_2010_L4_v-1.0.0.tif lccs_2010_L4_v-1.0.0_250m.tif
gdalwarp  -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" \
	-tr 250 -250 -te -2189542.2514926404692233 -4964936.3053171560168266 2468707.7485073595307767 -1047686.3053171562496573 \
	-t_srs EPSG:3577 -r mode -multi lccs_2010_L4_rgb_v-1.0.0.tif lccs_2010_L4_rgb_v-1.0.0_250m.tif
gdalwarp -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" \
	-tr 250 -250 -te -2189542.2514926404692233 -4964936.3053171560168266 2468707.7485073595307767 -1047686.3053171562496573 \
	-t_srs EPSG:3577 -r mode -multi lccs_2010_L3_rgb_v-1.0.0.tif lccs_2010_L3_rgb_v-1.0.0_250m.tif
```

## Creating a single band image with RAT ##

There are two stages to this process: converting the multi-band image to a single band image with a unique ID for each class and adding a RAT.

### Convert to single band image ###

Conversion to a single band image uses similar code as the colour scheme code. A CSV describes the values in each band and a unique ID for each class. When reading the multiband image band names are used to assign variables in the xarray so must be the same as in the level4 outputs.

To run use:
```
create_single_band_level4.py --pixel_ids australia_layers_cols_sorted.csv \
    lccs_2015_L4_1.0.0_250m.tif lccs_2015_L4_1.0.0_250m_single_band.img
```
The output must be ERDAS imagine (ending with .img) so a RAT can be added so output options for this are hardcoded but could easily be changed to export as a GeoTiff.

### Add RAT ###

This uses RSGISLib. The easiest way is to install to a separate conda environment and run through this.

The names for each class are taken from a CSV file. It will add names for all classes eve nif they don't exist in the classification.

```
add_level4_rat.py lccs_2015_L4_1.0.0_250m_single_band.img
```

Export RAT to CSV to give pixels for each class.
```python
from rsgislib import rastergis
rastergis.export2Ascii("lccs_2015_L4_v-1.0.0_single_band.kea", "lccs_2015_L4_v-1.0.0_single_band_cols.csv", ["LCCS_Code", "LCCS_Description", "Histogram"])
```

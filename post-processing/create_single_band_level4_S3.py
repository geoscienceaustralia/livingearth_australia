#!/usr/bin/env python
"""
A script to convert a individual L4 and multiband L3 tif files to a singleband L4.tif

The value of each band is defined in a CSV file.

For the script to work it assumes that the band names are the same as the level4 names used within the classification.

Call: python3 create_single_band_level4_s3.py path/to/folder/containing/tile/files output.tif --pixel_ids /livingearth_lccs/colour_schemes/lccs_l4_colour_scheme_golden_dark_au.csv
"""
import argparse
import os
import sys

import numpy
import pandas
import rasterio
import xarray as xr
from pathlib import Path
import boto3

from odc.aio import S3Fetcher
from le_lccs.le_classification import l4_base
from concurrent.futures import ThreadPoolExecutor, as_completed

S3_BUCKET = "lccs-dev"

def get_level4_pixel_id(classification_array, class_codes_csv):
    """"
    Gets a unique id for each level4 class based on an input CSV file

    :param xarray.Dataset classification_array: xarray dataset containing classified level 4 data layers.
    :param str colour_scheme_csv: The name of the colour scheme in LCCS_L4_COLOURS_CSV_DICT if using a
                                  standard colour scheme or the path to a CSV file for a non-standard
                                  scheme.

    """
    # Create a list of non-layer names in CSV.
    # For all other columns the heading is assumed to be the layer name
    non_layer_names = ["pixel_ID","LCCS_Code", "LCCS_Description", "Red", "Green", "Blue", "Alpha"]

    colour_scheme = pandas.read_csv(class_codes_csv)
    # Set up arrays for output
    pixel_id = numpy.zeros_like(classification_array[l4_base.LEVEL3_LAYER_NAME].values,
                                dtype=numpy.uint8)

    # Itterate through rows colour scheme
    for _, line in colour_scheme.iterrows():

        # If the column is a layer name add values to
        # 'layer_val_list'
        layer_val_list = []
        for layer, val in line.iteritems():
            if layer not in non_layer_names:
                layer_val_list.append((layer, val))

        # Using 'layer_val_list' get subset mask of elements (pixels) which
        # should be coloured
        subset = l4_base.get_colours_mask(classification_array, layer_val_list)
        # Colour output arrays
        pixel_id[subset] = line["pixel_ID"]

    return pixel_id

def dataset_paths(inputpath):
    """Extract S3 keys for all datasets in the provided path
    returns the set of unique dataset folders
    """

    s3 = S3Fetcher(aws_unsigned=True)
    allpaths =  [o.url for o in s3.find(inputpath, glob="*.tif")]
    return  set([str(Path(f).parent).replace("s3:/", "s3://") for f in allpaths])

def upload_to_s3(inputpath, single_band_output):
    s3 = boto3.client('s3')
    basepath = inputpath.replace("s3://lccs-dev/","")
    year = Path(basepath).parts[1]
    output_filepath = f"{basepath}/ga_ls_landcover_class_cyear_2_1-0-0_au_{year}-01-01_level4.tif"
    data = open(single_band_output, 'rb')
    print(f"Uploading {output_filepath} to {S3_BUCKET}")
    s3.put_object(Body=data, Bucket=S3_BUCKET, Key=output_filepath)

def multi_to_single_band(inputpath, class_codes_csv):
    """
    A function to calculate simple change between two geotiffs.
    """
    single_band_output = "/tmp/tmp.tif"
    print(f"Reading in data for {inputpath}")
    s3 = S3Fetcher(aws_unsigned=True)
    allpaths =  [o.url for o in s3.find(inputpath, glob="*.tif")]
    # Create list of all tif files in input path
    input_file_patterns = ['baregrad-phy-cat-l4d-au', 'canopyco-veg-cat-l4d', 'inttidal-wat-cat-l4a', 'lifeform-veg-cat-l4a', 'waterper-wat-cat-l4d-au', 'watersea-veg-cat-l4a-au', 'waterstt-wat-cat-l4a', 'L3_v']

    l4_xr = []
    # Iterate through files and append to list
    for p in allpaths:
        # check that the tif matches one of the desired inputs
        if any(pattern in str(p) for pattern in input_file_patterns):
            da = xr.open_rasterio(p)
            if 'descriptions' in da.attrs:
                da.name = da.descriptions[0]
            else:
                # split on month and day of date
                name = str(p).split("01-01_", 1)[1][0:-4]
                name = name.replace("-","_")
                da.name = name
            l4_xr.append(da)

    # Merge into single xarray dataset
    classification_data = xr.merge(l4_xr)
    print("Getting pixel IDs...")
    pixel_id = get_level4_pixel_id(classification_data, class_codes_csv=class_codes_csv)

    print("Writing out...")
    # Open file in rasterio so we can copy CRS and transform across to output file
    # TODO generalise this
    in_ds = rasterio.open(allpaths[0], "r")

    # Save out, set driver options based on output type.
    if os.path.splitext(single_band_output)[-1] == ".tif":
        additional_creation_options = {"compress" : "LZW",
                                       "tiled" : "True"}
        out_ds = rasterio.open(single_band_output, "w", driver="GTiff",
                           height=classification_data.dims["y"], width=classification_data.dims["x"],
                           count=1, dtype=numpy.uint8, nodata=0,
                           crs=in_ds.crs, transform=in_ds.transform, **additional_creation_options)
    out_ds.write(pixel_id)
    out_ds.set_band_description(1, "ID")

    in_ds.close()
    out_ds.close()
    upload_to_s3(inputpath, single_band_output)
    # Delete temporary file
    os.remove(single_band_output)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--inputpath", type=str,
                        help="Input 4 band L4 classification path")
    parser.add_argument("--outputimage", type=str,
                        help="Output single band image (should be .img format so RAT can be added.)")
    parser.add_argument("--pixel_ids", type=str,
                        help="CSV with pixel IDs for each Level 4 class")
    args = parser.parse_args()

    # Get the list of dataset folders
    datasets = dataset_paths(args.inputpath)
    print(f"Will modify total of {len(datasets)} datasets")
    # Convert to a single band image
    for ds in datasets:
        multi_to_single_band(ds, args.pixel_ids)
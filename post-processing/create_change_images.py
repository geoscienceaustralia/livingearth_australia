#!/usr/bin/env python
"""
Script to generate change images.

Runs each tile as a separate grid job on PML system.

Author: Dan Clewley
"""

import glob
import os
import subprocess

le_lccs_change = "/users/rsg/dac/usr/opt/conda/envs/dea-landcover/bin/le_lccs_change.py"
working_dir = "/users/rsg/dac/scratch_network/dea_landcover/continental_run"

grid_cmd = ["qsub",
            "-q", "lowpriority.q",
            "-P", "rsg",
            "-p", "-100",
            "-m", "n",
            "-b", "y",
            "-l", "throttle_dac=1",
            "-l", "mem_free=4G",
            "-e", os.path.join(working_dir, "change"),
            "-o", os.path.join(working_dir, "change")]

tiles_2010 = glob.glob(os.path.join(working_dir, "2010", "*L4_v-1.0.0.tif"))

for tile_2010 in tiles_2010:

    tile_id = os.path.basename(tile_2010).replace("L4_v-1.0.0.tif","").replace("2010","")
    try:
        tile_2015 = glob.glob(os.path.join(working_dir, "2015", f"*{tile_id}L4_v-1.0.0.tif"))[0]
    except IndexError:
        print(tile_id)
        tile_2015 = glob.glob(os.path.join(working_dir, "2015", 
                                           "*{tile_id}L4_v-1.0.0.tif".format(tile_id=tile_id.replace("_0_","21.0.0_"))))[0]

    out_l3_change = os.path.join(working_dir, "change", f"2010-2015{tile_id}L3_change_v-1.0.0.tif")
    out_l4_change = os.path.join(working_dir, "change", f"2010-2015{tile_id}L4_change_v-1.0.0.tif")
    out_observed_change_change = os.path.join(working_dir, "change", f"2010-2015{tile_id}observed_change_v-1.0.0.tif")

    change_cmd = [le_lccs_change, "--year1", tile_2010, "--year2", tile_2015,
                  "--output_l3_change_file_name", out_l3_change,
                  "--output_l4_change_file_name", out_l4_change,
                  "--output_observed_change_file_name", out_observed_change_change,
                  "--classification_scheme", "au"]

    if not os.path.isfile(out_observed_change_change):
        print(f"2010: {tile_2010}, 2015: {tile_2015}")

        change_submit_cmd = grid_cmd + ["-N", f"dea-landcover-change-{tile_id}"] + change_cmd
        print(" ".join(change_submit_cmd))

        subprocess.call(change_submit_cmd)

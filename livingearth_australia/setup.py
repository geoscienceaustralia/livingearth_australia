#!/usr/bin/env python
"""
Setup script for LCCS Australia Library. Use like this for Unix:

$ python setup.py install

"""

from setuptools import setup, find_packages

setup(name='livingearth_australia',
      version='1.0.0',
      description='A python module to be used in combination with livingearth_lccs, '
                  'with Australia specific land cover classification tools',
      packages=['le_plugins'],
      license='LICENSE.txt',
      url='https://bitbucket.org/au-eoed/livingearth_australia/src/master',
)

# Config file for running LE LCCS classification on AWS
# Needs:
# * master branch of livingearth_australia repo
# * master branch of livingearth_lccs repo
# * develop branch of dea-notebooks repo
#
# Set the following environmental variables
# export LE_LCCS_PLUGINS_PATH=/home/jovyan/development/livingearth_australia/le_plugins
# export PYTHONPATH=/home/jovyan/development/dea-notebooks/Scripts:/home/jovyan/development/livingearth_australia

# Set classification scheme ('lccs' is standard FAO LCCS, 'au' is Australia)
classification_scheme: au

# Set date for the classification (will be used for time series comparison)
processing_datetime: 2015-12-31
producer: ga.gov.au
product_family: lc
dataset_version: 1.0.0
collection_number: 3
platform: landsat-5,landsat-7,landsat-8
instrument: TM, ETM+, OLI
product_name: ga_ls_landcover_class_fyear_3
region_specific_lccs_code: au

# Specify extent (not used if getting from command line)
extent:
    # Sydney 46, 17
    min_x: 1728000
    max_x: 1824000
    min_y: -3840000
    max_y: -3744000

resolution: [30, -30]

crs: "EPSG:3577"

virtual_product_catalogue: &vp_cat "/opt/livingearth_australia/livingearth_australia/le_plugins/virtual_product_fy_cat.yaml"

Outputs:
    level3_data:
        export_class: gridded_export.LEExportGDAL
        output_file: "/home/jovyan/development/testing/sydney_l3_out.tif"
        variable_names_list: ["level3"]

    level3_rgb:
        export_class: gridded_export.LEExportGDAL
        output_file: "/home/jovyan/development/testing/sydney_l3_out_rgb.tif"

    level4_data:
        odc_indexable: True
        export_class: gridded_export.LEExportGDAL
        output_file: "/home/jovyan/development/testing/sydney_l4_out.tif"
        exclude_variable_names_list: ["Red", "Green", "Blue", "Alpha"]
        out_dtype: uint8

    level4_rgb:
        export_class: gridded_export.LEExportGDAL
        output_file: "/home/jovyan/development/testing/sydney_l4_out_rgb.tif"
        out_dtype: uint8

# Set up importer for each layer
L3layers:

    vegetat_veg_cat:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: vegetat_veg_cat
        transformations: [le_plugins.le_level1]
        aggregations: [le_plugins.FC_summary, le_plugins.water_mask, le_plugins.best_pixel_gmad]
        vp_catalogue: *vp_cat
        missing_layer_value: 1
        auxiliary_products:
            mangroves: ga_ls_mangrove_cover_fyear_3
            dem: ga_srtm_dem1sv1_0
        start_time: 2015-07-01
        end_time: 2016-06-30

    # WO input for aquatic areas
    wo_mask:
        ingest_class: gridded_ingest.LEIngestODC
        product: ga_ls_wo_fq_fyear_3
        measurements: [frequency]
        start_time: 2015-07-01
        end_time: 2016-06-30
        expstr: "frequency >= 0.2"

    # Intertidal extent model required for water state
    item_v2_mask:
        ingest_class: gridded_ingest.LEIngestODC
        product: item_v2
        measurements: [relative]
        expstr: "((relative >= 2) & (relative <= 8))"
        missing_layer_value: 0

    # Mangrove extent input for aquatic areas
    mangrove:
        ingest_class: gridded_ingest.LEIngestODC
        product: ga_ls_mangrove_cover_fyear_3
        measurements: [canopy_cover_class]
        start_time: 2015-07-01
        end_time: 2016-06-30
        expstr: "((canopy_cover_class == 1) | (canopy_cover_class == 2) | (canopy_cover_class == 3))"
        missing_layer_value: 0

    # Combine previously extracted layers to derive terrestrial/aquatic
    aquatic_wat_cat:
        ingest_class: xarray_maths
        expstr: "((wo_mask == 1) | (item_v2_mask == 1) | (mangrove == 1))"


    # Woody cover fraction mask (wcf) and threshold by >= 0.004 to get mask for bare areas
    wcf_mask:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: wcf_mask
        transformations: [le_plugins.wcf_mask, le_plugins.woody_cover]
        aggregations: [le_plugins.best_pixel_gmad]
        vp_catalogue: *vp_cat
        start_time: 2015-07-01
        end_time: 2016-06-30

    # Cultivated/managed area classified using ML(randomforest)
    sklearn_cultivated_classification:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: sklearn_cultivated_classification
        transformations: [le_plugins.MLClassification]
        aggregations: [le_plugins.best_pixel_gmad]
        vp_catalogue: *vp_cat
        missing_layer_value: 1
        start_time: 2015-07-01
        end_time: 2016-06-30

    # Filter cultivated to ensure only herbaceous land cover is classified as cultivated
    cultman_agr_cat:
        ingest_class: xarray_maths
        expstr: "((sklearn_cultivated_classification == 1) & (lifeform_veg_cat == 2))"

    # Artificial surfaces classified using ML(tensorflow)
    tf_urban_classification:
        ingest_class: gridded_ingest.LEIngestODCV
        transformations: [le_plugins.tf_urban]
        aggregations: [le_plugins.best_pixel_gmad]
        virtual_product: tf_urban
        vp_catalogue: *vp_cat
        start_time: 2015-07-01
        end_time: 2016-06-30

    # WO summary mask for bare areas
    wo_sum_mask:
        ingest_class: gridded_ingest.LEIngestODC
        product:  ga_ls_wo_fq_myear_3
        measurements: [frequency]
        expstr: "frequency >= 0.01"

    # Mask on Urban Centre and Locality (UCL) Ed. 2016 for bare areas
    UCL_mask:
        ingest_class: gridded_ingest.LEIngestGDAL
        input_file: s3://dea-public-data/projects/LCCS/urban_mask.tif
        band: 1

    # Mask Tensor Flow urban classification combining two MAD layers and wcf_mask to derive artificial surface/bare areas
    artific_urb_cat:
        ingest_class: xarray_maths
        expstr: "((tf_urban_classification == 1) & (wcf_mask == 1) & (wo_sum_mask == 0) & (UCL_mask == 0))"

L4layers:

    # Lifeform returns only woody and non woody categories
    lifeform_veg_cat:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: lifeform
        transformations: [le_plugins.lifeform, le_plugins.woody_cover]
        vp_catalogue: *vp_cat
        start_time: 2015-07-01
        end_time: 2016-06-30

    # Canopy cover (%) derived from fractional cover annual percentiles product
    canopyco_veg_con:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: canopycover
        transformations: [le_plugins.canopycover]
        vp_catalogue: *vp_cat
        start_time: 2015-07-01
        end_time: 2016-06-30

    # Water seasonality derived from WO summary product
    watersea_veg_cat:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: waterseasonality
        transformations: [le_plugins.waterseasonality]
        vp_catalogue: *vp_cat
        start_time: 2015-07-01
        end_time: 2016-06-30

    # Combine previously extracted layers to derive water state
    waterstt_wat_cat:
        ingest_class: xarray_maths
        expstr: "((wo_mask == 1) | (item_v2_mask == 1))"

    # Water persistence derived from WO summary product
    waterper_wat_cin:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: waterpersistence
        transformations: [le_plugins.waterpersistence]
        vp_catalogue: *vp_cat
        start_time: 2015-07-01
        end_time: 2016-06-30

    # Intertidal extent model input required for intertidal areas
    inttidal_wat_cat:
        ingest_class: xarray_maths
        expstr: "(item_v2_mask == 1) * 3"

    # Bare gradation (%) derived from fractional cover product
    baregrad_phy_con:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: baregradation
        transformations: [le_plugins.baregradation]
        vp_catalogue: *vp_cat
        start_time: 2015-07-01
        end_time: 2016-06-30
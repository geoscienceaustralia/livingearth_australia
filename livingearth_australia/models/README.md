# Model Parameters for running LCCS classification for Australia #

## Cultivated ##

* `cultivated_sklearn_model.pickle` - sklearn model for classifying cultivated vegetation

## Urban ##

* `geomedian_6bands_5classes_classification.h5` - saved TensorFlow model
* `geomedian_6bands_5classes_standarise_parameters` - standerdisation paramers to be applied to geomedian after scaling between 0-1


from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy as np
from itertools import groupby
from datacube.storage import masking

class waterpersistence(Transformation):
    
    def consecutive_count_water(self, water_numpy, consecutive_numpy, consec_msk_val=1):
        '''
        function to get consective count of water (1) for each pixel across the time series
        and normalise based on number of observed months

        :param numpy.array water_numpy: 3-D array of tv[z, y, x]
        :param numpy.array consecutive_numpy: Bool output array for water (1) and not water (0)
        :param float consec_msk_val: What value to calculcate consectutives on
                                     Default is water (1)     
        '''

        for y in range(water_numpy.shape[1]):
            for x in range(water_numpy.shape[2]):
                # If all the values are nan then just set output to nan and carry on
                pixel = water_numpy[:, y, x]

                if np.all(np.isnan(pixel)):
                    consecutive_numpy[y,x] = np.nan
                    continue            

                # Get counts of consecutive water classifications
                # returns an array with consecutive counts of 1's
                counts = [len(list(group)) for label, group in groupby(pixel[(~np.isnan(pixel))]) if label == 1]

                # Get counts of valid months observations (i.e. just 0's or 1's)
                # returns an array with consecutive counts of 0's or 1's
                obs_counts = [len(list(group)) for label, group in groupby(pixel[(~np.isnan(pixel))]) if label >= 0]

                # if counts is empty array, output 0
                if not counts:
                    consecutive_numpy[y,x] = 0

                # if counts is not empty array, give the max consecutive counts of 1's,
                # divided by sum of obs_counts (i.e. number of months of observations),
                # multipled by 12 (to get value as how many months)
                else:
                    consecutive_numpy[y,x] = (max(counts) / sum(obs_counts))*12

        return consecutive_numpy
 
    def compute(self, data):

        # Select clear and water pixels - binary (128: clear and wet, 132: clear, wet & sea 0: clear)
        water = ((data.water == 128) | (data.water == 132)).to_dataset()
        clear = ((data.water == 128) | (data.water == 132) | (data.water == 0)).to_dataset()

        # Group by month - total number of observations
        water_month = water.groupby('time.month').sum(dim='time')   
        clear_month = clear.groupby('time.month').sum(dim='time') 

        # Calculate frequency of water observations within the month
        freq = water_month/clear_month

        # Threshold frequency in month to get water or not
        # Values greater than 0.5 kept
        water_dominant = freq['water'] > 0.5
        # Converted to binary
        water_mask = water_dominant.where(freq['water'] >= 0)

        # Make values of > 0.5 == 1 (end result in binary water mask)
        water_mask_drop = np.where(water_mask > 0.5, 1, water_mask)
        water_mask_xr = xr.DataArray(water_mask_drop, coords=water_mask.coords, dims=water_mask.dims)

        # Pull water as numpy array
        water_numpy = water_mask_xr.values

        # Set up array for output (faster to pre-allocate for numba).
        # Needs to be float32 for NaN
        consecutive_numpy = np.empty((water_numpy.shape[1], water_numpy.shape[2]), dtype=np.float32)

        # Run function to get array of with 1 where number of consecutive values for different
        # dates is >= required_consecutive
        consecutive_numpy = self.consecutive_count_water(water_numpy, consecutive_numpy)

        # reduce original array to 2D
        # using max here but not important as we just want to use shape/attributes
        water_reduce = water_mask.max(dim='month')

        # create new data array with same dimensions as original (output is 0-12 giving max consecutive counts)
        water_summary = xr.DataArray(consecutive_numpy, coords=water_reduce.coords, dims=water_reduce.dims)  
        return water_summary.to_dataset(name='waterper_wat_cin')

    def measurements(self, input_measurements):
        return {'waterper_wat_cin': Measurement(name='waterper_wat_cin', dtype='float32', nodata=float('nan'), units='1')}

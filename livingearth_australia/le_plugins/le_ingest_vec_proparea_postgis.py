'''
This is an unused plugin for Australia implementation LCCS (v 1.0.0).
This plugin was part of a suite of plugins (le_ingest_vector.py, le_ingest_vec_proparea.py,
le_ingest_vec_proparea_postgis.py) for integration of Open Street Map (OSM) to derive urban density at
LCCS level 4.

This plugin is being archived as it may be relevant in future and details an implementation of
an approach using an open-source dataset with a method relevant to DEA continental products.
'''

import psycopg2
from psycopg2.sql import SQL, Identifier

from le_lccs.le_ingest.gridded_ingest import LEIngestGridded

# Set up a class to inherit from 'LEIngestGridded'
class LEIngest_PostgisVecIntersectPropArea(LEIngestGridded):

    def read_to_xarray(self, postgis_dsn, geometry_column, variable_name, table_name, srs_id, **kwargs):
        """
        Function to take a vector, rasterise it and then read into an xarray
        """

        connection = psycopg2.connect(postgis_dsn)
        cursor = connection.cursor()


        query_args = {
            'xmin': self.target_min_x,
            'xmax': self.target_max_x,
            'ymin': self.target_min_y,
            'ymax': self.target_max_y,
            'srs_id': srs_id
        }

        cursor.execute('SELECT ST_Area(ST_MakeEnvelope(%(xmin)s, %(ymin)s, %(xmax)s, %(ymax)s, %(srs_id)s));', query_args)
        bounds_area, = cursor.fetchone()

        # Switch on buffer to decide if we're dealing with polygons on lines
        if 'buffer' not in kwargs:
            # Calculate area of polygons inside grid area
            query = '''
            select SUM(ST_Area(ST_Intersection({geometry_column}, ST_MakeEnvelope(%(xmin)s, %(ymin)s, %(xmax)s, %(ymax)s, %(srs_id)s))))
            from {table_name}
            where wkb_geometry && ST_MakeEnvelope(%(xmin)s, %(ymin)s, %(xmax)s, %(ymax)s, %(srs_id)s);
            '''
        else:
            # Calculate area of buffered lines inside grid area
            query = '''
            select SUM(ST_Area(ST_Buffer(ST_Intersection({geometry_column}, ST_MakeEnvelope(%(xmin)s, %(ymin)s, %(xmax)s, %(ymax)s, %(srs_id)s)), %(buffer)s)))
            from {table_name}
            where {geometry_column} && ST_MakeEnvelope(%(xmin)s, %(ymin)s, %(xmax)s, %(ymax)s, %(srs_id)s);
            '''
            query_args['buffer'] = float(kwargs['buffer'])

        query = SQL(query).format(table_name=Identifier(table_name), geometry_column=Identifier(geometry_column))

        cursor.execute(query, query_args)
        vec_area, = cursor.fetchone()

        cursor.close()
        connection.close()

        print("Vector intersection area = {}".format(vec_area))
        vec_area = 0 if vec_area is None else vec_area
        # Define constant for the proportion of the tile which intersect with the vector layer.
        print("Percentage intersection area = {}".format((vec_area/bounds_area)*100))
        return self.set_to_constant_value(variable_name, (vec_area/bounds_area)*100)

from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy
import copy
from datacube.storage import masking

class baregradation(Transformation):
    def compute(self, data):
        
        masked_fc = masking.mask_invalid_data(data)
        masked_fc['bs'] = masked_fc.bs.where(masked_fc.ue < 30)
        
        fc_med = masked_fc.bs.median(axis=0)
        baregradation = numpy.where(((fc_med <= 100) & (fc_med >= 1)), fc_med, 0)
        out = xr.Dataset({'baregrad_phy_con': (data.bs[0].dims, baregradation)}, coords=fc_med.coords, attrs=fc_med.attrs)

        return out

    def measurements(self, input_measurements):
        return {'baregrad_phy_con': Measurement(name='baregrad_phy_con', dtype='float32', nodata=float('nan'), units='1')}

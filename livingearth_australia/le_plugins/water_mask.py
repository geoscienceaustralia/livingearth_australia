from datacube.virtual import construct, Transformation, Measurement

class water_mask(Transformation):
    '''
    Ensure the water mask has correct datetime
    '''
    def compute(self, data):
        return data.drop(['time']).squeeze()

    def measurements(self, input_measurements):
        return {'wo_mask': Measurement(name='wo_mask', dtype='bool', units='1')}

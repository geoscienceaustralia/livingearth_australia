from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy as np
from itertools import groupby
from datacube.storage import masking


class FC_summary(Transformation):
    
    def consecutive_count_veg(self, tv_numpy, consecutive_numpy, required_consecutive, consec_msk_val=1):
        '''
        function to get consective count of veg (1) for each pixel across the time series

        :param numpy.array tv_numpy: 3-D array of tv[z, y, x]
        :param numpy.array consecutive_numpy: Bool output array for veg (1) and non veg (0)
        :param float required_consecutive: How many consecutive 1's to be classified as veg?
                                           FAO LCCS definition is required_consecutive = 2.
        :param float consec_msk_val: What value to calculcate consecutives on?
                                     Default is veg (1)      
        '''

        for y in range(tv_numpy.shape[1]):
            for x in range(tv_numpy.shape[2]):
                # If all the values are nan then just set output to nan and carry on
                pixel = tv_numpy[:, y, x]

                if np.all(np.isnan(pixel)):
                    consecutive_numpy[y, x] = np.nan
                    continue            

                # Get counts of consecutive veg classifications
                counts = [len(list(group)) for label, group in groupby(pixel[(~np.isnan(pixel))]) if label == 1]

                if not counts:
                    consecutive_numpy[y,x] = 0
                elif max(counts) >= required_consecutive:
                    consecutive_numpy[y,x] = 1
                else:
                    consecutive_numpy[y,x] = 0
        
        return consecutive_numpy

    def consecutive_count_nonveg(self, tv_numpy, consecutive_numpy, required_consecutive, consec_msk_val=1):
        '''
        function to get consective count of non veg (0) for each pixel across the time series

        :param numpy.array tv_numpy: 3-D array of tv[z, y, x]
        :param numpy.array consecutive_numpy: Bool output array for veg (1) and non veg (0)
        :param float required_consecutive: How many consecutive 0's to be classified as non veg?
                                           FAO LCCS definition is required_consecutive = 2.
        :param float consec_msk_val: What value to calculcate consecutives on?
                                     Default is non veg (0) 
        '''

        for y in range(tv_numpy.shape[1]):
            for x in range(tv_numpy.shape[2]):
                # If all the values are nan then just set output to nan and carry on
                pixel = tv_numpy[:, y, x]

                if np.all(np.isnan(pixel)):
                    consecutive_numpy[y, x] = np.nan
                    continue
                    
                # Get counts of consecutive non veg classifications (hence label == 0)
                counts = [len(list(group)) for label, group in groupby(pixel[(~np.isnan(pixel))]) if label == 0]

                # Inverse values from consecutive_count_veg to make non veg
                if not counts:
                    consecutive_numpy[y, x] = 1
                elif max(counts) >= required_consecutive:
                    consecutive_numpy[y, x] = 0
                else:
                    consecutive_numpy[y, x] = 1

        return consecutive_numpy


    def compute(self, data):
        data_nan = masking.mask_invalid_data(data)

        # Create water and low-quality fc masks
        no_water = np.invert( (data['water'] == 128) | (data['water'] == 132) )
        high_ue = (data['ue'] > 30) & no_water
        
        # Set to null areas with high ue, but no water
        data_nan = data_nan.where(np.invert(high_ue))
        
        # Don't need this anymore
        data_nan = data_nan.drop_vars("ue")
        del high_ue

        # Where there's water, set pv to 0
        data_nan['pv'] = data_nan.pv.where(no_water, 0)
        data_nan['npv'] = data_nan.npv.where(no_water, 0)

        # Where there's water, set bs to 100
        # this artificial inflation on bs ensures water areas are classified as non-veg
        data_nan['bs'] = data_nan.bs.where(no_water, 100)

        # don't need this anymore
        data_nan = data_nan.drop_vars("water")
        del no_water

        data_month = data_nan.groupby('time.month').median(dim='time')    

        # Create mask where pv > bs
        pv_dominant = data_month['pv'] > data_month['bs']
        pv_dominant_nan = pv_dominant.where((data_month['pv'] >= 0) & (data_month['bs'] >= 0))

        # Create mask where npv > bs
        npv_dominant = data_month['npv'] > data_month['bs']
        npv_dominant_nan = npv_dominant.where((data_month['npv'] >= 0) & (data_month['bs'] >= 0))

        # Veg where pv or npv are greater than bs
        tv_mask = (pv_dominant_nan + npv_dominant_nan)

        # make values of 2 == 1 (end result in binary veg mask)
        tv_mask_drop = np.where(tv_mask == 2, 1, tv_mask)
        tv_mask_xr = xr.DataArray(tv_mask_drop, coords=tv_mask.coords, dims=tv_mask.dims)
        
        ##### veg #####
        # Executing consecutive_count function #
        # veg and non veg = 2 consecutive months
        required_consecutive = 2

        # Pull tv as numpy array
        tv_numpy = tv_mask_xr.values

        # Set up array for output (faster to pre-allocate for numba).
        # Needs to be float32 for NaN
        consecutive_numpy = np.empty((tv_numpy.shape[1], tv_numpy.shape[2]), dtype=np.float32)

        # For veg - run function to get array of with 1 where number of consecutive values for
        # different dates is >= required_consecutive
        consecutive_numpy_veg = self.consecutive_count_veg(tv_numpy, consecutive_numpy, required_consecutive)

        # Reduce original array to 2D
        # using max here but not important as we just want to use shape/attributes
        tv_reduce = tv_mask.max(dim='month')

        # Create new data array with sampe dimensions as original
        tv_summary_veg = xr.DataArray(consecutive_numpy_veg, coords=tv_reduce.coords, dims=tv_reduce.dims)

        ##### non veg #####
        # Executing consecutive_count function #
        # veg and non veg = 2 consecutive months
        required_consecutive = 2

        # Pull tv as numpy array
        tv_numpy = tv_mask_xr.values

        # Set up array for output (faster to pre-allocate for numba).
        # Needs to be float32 for NaN
        consecutive_numpy = np.empty((tv_numpy.shape[1], tv_numpy.shape[2]), dtype=np.float32)

        # For non veg - run function to get array of with 0 where number of consecutive values for
        # different dates is >= required_consecutive
        consecutive_numpy_nonveg = self.consecutive_count_nonveg(tv_numpy, consecutive_numpy, required_consecutive)

        # Reduce original array to 2D
        # using max here but not important as we just want to use shape/attributes
        tv_reduce = tv_mask.max(dim='month')

        # Create new data array with sampe dimensions as original
        tv_summary_nonveg = xr.DataArray(consecutive_numpy_nonveg, coords=tv_reduce.coords, dims=tv_reduce.dims)

        # Combine veg and non veg masks (to ensure no data is correctly identified)
        tv_combine = tv_summary_veg + tv_summary_nonveg

        # Make values of 2 == 1 (end result in binary veg mask)
        tv_combine_drop = np.where(tv_combine == 2, 1, tv_combine)

        # Create new data array with sampe dimensions as original
        tv_summary = xr.DataArray(tv_combine_drop, coords=tv_combine.coords, dims=tv_combine.dims)

        return tv_summary.to_dataset(name='fc_veg')

    def measurements(self, input_measurements):
        return {'fc_veg': Measurement(name='fc_veg', dtype='float32', nodata=float('nan'), units='1')}
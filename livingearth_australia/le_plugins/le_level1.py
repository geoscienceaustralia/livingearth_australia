from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy as np
import copy
from itertools import groupby
from datacube.storage import masking
from datacube.testutils.io import rio_slurp_xarray
import datacube

dc = datacube.Datacube()


class le_level1(Transformation):

    mangroves_product = None
    dem_product = None

    @staticmethod
    def set_auxiliary_products(auxiliary_products):
        for key, value in auxiliary_products.items():
            if "mangroves" in key:
                le_level1.mangroves_product = value
            elif "dem" in key:
                le_level1.dem_product = value

    def compute(self, data):
        """
        combine all level 1 inputs: fc_veg, wo_mask, saltpan_mask, mudflat_mask, DEM, mangrove
        """

        if self.mangroves_product is None or self.dem_product is None:
            raise ValueError(
                "Either or both of the following products are not defined for level1: [mangroves, dem]"
            )

        year = str(data.time.data[0])
        data = data.squeeze(dim="time", drop=True)

        # generate DEM mask
        DEM = dc.load(
            product=le_level1.dem_product, like=data, measurements=["dem_h"]
        ).squeeze(dim="time", drop=True)
        DEM_mask = np.where((DEM["dem_h"].values <= 6), 1, 0)

        # calculate SI5, mudflat & saltpan masks
        si5 = (data["blue"] * data["red"]) / (data["green"])
        mudflat_mask = np.where(si5 > 1000, 1, 0)
        mudflat_mask_nan = np.where((data["red"] == 0), 1, mudflat_mask)
        saltpan_mask = np.where(si5 < 1500, 1, 0)
        saltpan_mask_nan = np.where((data["red"] == 0), 1, saltpan_mask)

        # combine masks
        veg = (
            (data.fc_veg.data)
            * (1 - (data.wo_mask.data))
            * (saltpan_mask_nan)
            * (1 - (mudflat_mask_nan * DEM_mask))
        )

        # load mangroves
        mangroves = dc.load(
            product=le_level1.mangroves_product,
            like=data,
            measurements=["canopy_cover_class"],
            time=year,
        )
        if mangroves:
            mangroves = mangroves.squeeze(dim="time", drop=True)
            # make mask of all mangrove camopy classes
            mangrove_mask = (
                (mangroves["canopy_cover_class"] == 1)
                + (mangroves["canopy_cover_class"] == 2)
                + (mangroves["canopy_cover_class"] == 3)
            ) * 1
            # veg unchanged where mangroves are false, where mangroves are true add veg (change to 1 from 0)
            veg = np.where(mangrove_mask == 1, mangrove_mask, veg)

        vegetat_veg_cat = xr.DataArray(
            veg.astype("float32"), coords=data.coords, dims=data.fc_veg.dims
        )
        return vegetat_veg_cat.to_dataset(name="vegetat_veg_cat")

    def measurements(self, input_measurements):
        return {
            "vegetat_veg_cat": Measurement(
                name="vegetat_veg_cat",
                dtype="float32",
                nodata=float("nan"),
                units="1",
            )
        }

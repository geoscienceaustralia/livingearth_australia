
from datacube.virtual import construct, Transformation, Measurement

import datacube
import numpy as np
import tensorflow.keras.models as models
import dea_tools.classification as classificationtools
from dea_tools.bandindices import calculate_indices
from datacube.utils.cog import write_cog


class tf_urban(Transformation):
    """
    Class to classify urban vegetation using Peter Tan's non-linear spectral unmixing model
    Example outputs can be found here: /g/data/r78/LCCS_Aberystwyth/test_sites_peter/outputs
    Example code to apply the model shown here: https://bitbucket.org/au-eoed/livingearth_lccs_development_tests/src/master/notebooks/artificial_surfaces/unmix_geomediandata.ipynb

    The first band is vegetation fraction, the second is bare surface and the third is infastructure

    Data can be inspected like:

    from spectral import *
    # Grabs the third band
    img = open_image('Perth_2015_gre_bare_inf_fractions.hdr').read_band(2)
    """

    model = None

    def __init__(self, tf_model, **settings):
        """
        Takes an existing model saved out as an h5 file.
        """
        # Load in model
        self.model = models.load_model(tf_model)

    def compute(self, data):
        """
        Function to apply TensorFlow model to classify urban
        """
        # drop Mad bands grom GeoMad
        data = data.drop_vars(['sdev', 'edev', 'bcdev', 'count'])
        
        data = data.compute()

        scaled_data = data * (1.0/10000)

        # Calculate indices problem with drop
        indices = calculate_indices(scaled_data, ['TCB', 'MNDWI', 'NDBI', 'NDVI', 'NDTI'], collection='ga_ls_2', inplace=True)
        
        # Flatten data
        flattened = classificationtools.sklearn_flatten(indices)

        # Predict using model
        fractions = self.model.predict(flattened)

        # Threshold fractions, the "infastructure" fraction is in the third band.
        # & (fractions[:,2] < 0.6) for thresholding very high urban fractions
        predicted = np.where(((fractions[:, 0] < .5) & (
            fractions[:, 2] > 0.08) & (fractions[:, 1] < 0.75)), 1, 0)

        # Reshape to same size as input xarray
        predicted = classificationtools.sklearn_unflatten(
            predicted, indices)

        # Remove time dimension
        predicted = predicted.squeeze("time").drop_vars("time")

        # Write out raw fractions as a tif
        # Reshape to same size as input xarray
#         fractions = dea_classificationtools.sklearn_unflatten(fractions, indices)

        # Remove time dimension
#         fractions = fractions.squeeze("time").drop_vars("time").transpose()
#         fractions = assign_crs(fractions, crs='epsg:3577')
#         name = str(np.random.randint(10000))
#         write_cog(fractions, fname=f'{name}.tif', overwrite=True)

        # Return as a dataset
        # TODO: Neet to transpose to get the correct dimensions due to something in TF mmodel, should be a nicer fix
        return predicted.to_dataset(name='tf_urban_classification').transpose()

    def measurements(self, input_measurements):
        return {'tf_urban_classification': Measurement(name='tf_urban_classification',
                                                       dtype='float32', nodata=float('nan'), units='1')}

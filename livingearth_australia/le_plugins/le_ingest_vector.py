'''
This is an unused plugin for Australia implementation LCCS (v 1.0.0).
This plugin was part of a suite of plugins (le_ingest_vector.py, le_ingest_vec_proparea.py,
le_ingest_vec_proparea_postgis.py) for integration of Open Street Map (OSM) to derive urban density at
LCCS level 4.

This plugin is being archived as it may be relevant in future and details an implementation of
an approach using an open-source dataset with a method relevant to DEA continental products.

A version of this plugin is used in LCCS le_plugins documentation as an example for future users.
'''

import logging
import os
import shutil
import subprocess
import tempfile

from le_lccs.le_ingest.gridded_ingest import LEIngestGridded

class LEIngestVector2Raster(LEIngestGridded):
    """
    Class to ingest a vector into Living Earth LCCS code by rasterising and reading to
    xarray.
    """
    def read_to_xarray(self, input_vector, variable_name,
                       **kwargs):
        """
        Function to take a vector, rasterise it and then read into an xarray
        """
        # Create temporary output folder
        temp_dir = tempfile.mkdtemp(prefix="lccs_ingest")
        temp_vector_sub = os.path.join(temp_dir, "vector_sub.shp")
        temp_out_raster = os.path.join(temp_dir, "raster.tif")

        # Run OGR subset command
        subset_cmd = ["ogr2ogr", "-clipdst"]
        subset_cmd.extend([str(self.target_min_x), str(self.target_min_y),
                           str(self.target_max_x), str(self.target_max_y)])
        subset_cmd.extend([temp_vector_sub, input_vector])
        logging.info("Clipping vector using: ".format(" ".join(subset_cmd)))
        cmd_out = subprocess.check_output(subset_cmd)
        logging.info(cmd_out)

        # Run gdalrasterize command
        rasterise_cmd = ["gdal_rasterize", "-te"]
        rasterise_cmd.extend([str(self.target_min_x), str(self.target_min_y),
                              str(self.target_max_x), str(self.target_max_y)])
        rasterise_cmd.extend(["-tr", str(self.target_pixel_size_x), str(self.target_pixel_size_y)])
        rasterise_cmd.extend(["-burn", "1"])
        rasterise_cmd.extend([temp_vector_sub, temp_out_raster])

        logging.info("Creating raster using: ".format(" ".join(rasterise_cmd)))
        cmd_out = subprocess.check_output(rasterise_cmd)
        logging.info(cmd_out)

        # Read raster using read_data_from_gdal function inherited from LEIngestGridded
        out_xarray = self.read_data_from_gdal(temp_out_raster, variable_name)

        # Remove temporary raster
        shutil.rmtree(temp_dir)

        return out_xarray

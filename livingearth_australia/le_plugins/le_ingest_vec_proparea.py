'''
This is an unused plugin for Australia implementation LCCS (v 1.0.0).
This plugin was part of a suite of plugins (le_ingest_vector.py, le_ingest_vec_proparea.py,
le_ingest_vec_proparea_postgis.py) for integration of Open Street Map (OSM) to derive urban density at
LCCS level 4.

This plugin is being archived as it may be relevant in future and details an implementation of
an approach using an open-source dataset with a method relevant to DEA continental products.
'''

import os
import geopandas
from shapely.geometry import Polygon
import subprocess
import tempfile

from le_lccs.le_ingest.gridded_ingest import LEIngestGridded

# Set up a class to inherit from 'LEIngestGridded'
class LEIngest_VecIntersectPropArea(LEIngestGridded):

    def read_to_xarray(self, input_vector, input_vector_name, variable_name, **kwargs):
        """
        Function to take a vector, rasterise it and then read into an xarray
        """
        # bounding box
        bounds = Polygon([(self.target_min_x, self.target_min_y), 
                          (self.target_min_x, self.target_max_y), 
                          (self.target_max_x, self.target_max_y), 
                          (self.target_max_x, self.target_min_y)])
        print("bounds area = {}".format(bounds.area))
        
        # Open vector - check is a layer name has been specified.
        vec_gp_obj = geopandas.read_file(input_vector, layer=input_vector_name)
        
        # Check whether there are geometries which intersect the tile.
        if not (vec_gp_obj['geometry'].intersection(bounds)).any():
            vec_area = 0
            print("No vector features present in AOI")
        else:
            # There are geometries which intersect the tile
            # Perform intersection with tile bounds.
            vec_gp_obj = vec_gp_obj['geometry'].intersection(bounds)
            # If buffer specified then perform buffering.
            if 'buffer' in kwargs:
                vec_gp_obj = vec_gp_obj.buffer(kwargs['buffer'])
            
            # Calculate the area of the features.
            vec_area = 0
            for vec_feat in vec_gp_obj[vec_gp_obj.geometry.area>0]:
                vec_area += vec_feat.area
        print("Vector intersection area = {}".format(vec_area))
        
        # Define constant for the proportion of the tile which intersect with the vector layer.
        print("Percentage intersection area = {}".format((vec_area/bounds.area)*100))
        return self.set_to_constant_value(variable_name, (vec_area/bounds.area)*100)

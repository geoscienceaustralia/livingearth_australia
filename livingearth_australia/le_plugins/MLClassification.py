"""
Apply ML classification to ODC data as a virtual product

Uses dask to run in parallel

Dan Clewley
2019-11-06

"""

import joblib
import pickle
import numpy
import sklearn
import xarray

import logging

from datacube.virtual import construct, Transformation, Measurement
from dea_tools.bandindices import calculate_indices

class MLClassification(Transformation):

    def __init__(self, model_file, **settings):
        """
        Takes an existing model saved out as a joblib file.
        To provide some sanity checks also have variables and output class names in
        same file and classifier.

        Requires a dictionary with:

        variables : list of variables used for the classification
        classes : dictionary of class names and corresponding codes
        classifier : classification model, expects to be able to run with model.predict(X)

        """

        # Load model
        if 'pickle' in model_file:
            with open(model_file, "rb") as f:
                self.ml_model_dict = pickle.load(f)
        if 'joblib' in model_file:
            with open(model_file, "rb") as f:
                self.ml_model_dict = joblib.load(f)

        # Check we've loaded a dictionary
        if not isinstance(self.ml_model_dict, dict):
            raise TypeError("Was expecting to unpickle as a dictionary. Got: "
                            "{}",format(type(self.ml_model)))

        # Get the variables we were expecting
        try:
            self.ml_variables = self.ml_model_dict["variables"]
            self.ml_classes = self.ml_model_dict["classes"]
            self.ml_classifier = self.ml_model_dict["classifier"]

        except KeyError:
            raise KeyError("Couldn't find all expected keys in dictionary")

        # Check we have an instance of a sklearn classifier
        # This should cover all of them
        if not isinstance(self.ml_classifier, sklearn.base.ClassifierMixin):
            raise TypeError("Was expecting to load an instance of sklearn.base.ClassifierMixin"
                            " got {}".format(type(self.ml_classifier)))


    def compute(self, data):
        """
        Apply a classification to data

        Uses dask to run in parallel

        """
        indices = ['BUI', 'BAI', 'EVI', 'SAVI', 'LAI', 'TCG', 'NDMI', 'BSI', 'NDVI', 'TCW']
        data = calculate_indices(data, indices, collection='ga_ls_3', inplace=True)
        # Set up a list of input data using variables passed in
        input_data = []
        for var_name in self.ml_variables:
            try:
                input_data.append(data[var_name])
            except KeyError:
                raise KeyError("Classification requires {} but this isn't available"
                               "".format(var_name))

        # Run through classification. Need to expand and have a separate dataframe for
        # each variable so chunking in dask works.
        out_class = self._get_class(*input_data).compute()

        # Classification returns a numpy array so save out as a dataframe
        out_class_ds = xarray.Dataset(
            {"classification" : (data[self.ml_variables[0]].dims, out_class.data)},
            coords=data.coords)
        return out_class_ds.squeeze().drop_vars("time")

    def measurements(self, input_measurements):
        return {"Classification": Measurement(name="Classification", dtype="uint8",
                                              nodata=0, units="1")}

    def _get_class_ufunc(self, *args):
        """
        ufunc to apply classification to chunks of data
        """
        input_data_flattened = []
        for data in args:
            input_data_flattened.append(data.flatten())

        # Flatten array
        input_data_flattened = numpy.array(input_data_flattened).transpose()

        # Mask out no-data in input (not all classifiers can cope with Inf or NaN values)
        input_data_flattened = numpy.where(numpy.isfinite(input_data_flattened),
                                           input_data_flattened, 0)

        # Actually apply the classification
        out_class = self.ml_classifier.predict(input_data_flattened)


        # Mask out NaN or Inf values in results
        out_class = numpy.where(numpy.isfinite(out_class), out_class, 0)

        # Reshape when writing out
        return out_class.reshape(data.shape)

    def _get_class(self, *args):
        """
        Apply classification to xarray DataArrays.

        Uses dask to run chunks at a time in parallel

        """
        out = xarray.apply_ufunc(self._get_class_ufunc, *args,
                                 dask='parallelized', output_dtypes=[numpy.uint8])

        return out

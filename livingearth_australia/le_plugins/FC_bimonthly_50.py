'''
This is an unused plugin for Australia implementation LCCS (v 1.0.0).
This plugin was tested as an alternative approach to the consecutive counting function using Fraction Cover (FC).
Specifically, we utilise the bimonthly approach developed by Albert Van Dijk (ANU) to reduce noise and 
capture useful variability in the FC endmembers. Importantly we included all endmembers, however a failsafe
exists to ensure the for non veg to be classified the PV and NPV endmembers must not be > 50.

This plugin is being archived as it may be relevant in future and details an implementation of
a hybrid approach using a published method relevant to DEA continental products.

For implementation in LCCS level 3, the virtual_product_cat.yaml data inputs are identical to the consective counting
function of FC_summary.py, as below

    fc_bimonthly_50:
        recipe:
            transform: FC_bimonthly_50
            input:
                juxtapose:
                    - *ls_fc_wo_masked_recipe
                    - *wo_daily_recipe

The implementation for the l3_vp_template.yaml are alsp indentical to FC_summary.py, as below

    fc_bimonthly_50:
        ingest_class: gridded_ingest.LEIngestODCV
        virtual_product: fc_bimonthly_50
        transformations: [le_plugins.FC_bimonthly_50]
        vp_catalogue: *vp_cat
        missing_layer_value: 1
        start_time: 2015-01-01
        end_time: 2015-12-31
'''

from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy as np
from itertools import groupby
from datacube.storage import masking


class FC_bimonthly_50(Transformation):

    def compute(self, data):
        # mask_invalid_data used here as some of the FC fractions may contain -ve values so this makes sure everything goes from 0-100%
        data_nan = masking.mask_invalid_data(data)

        # Create water and low-quality fc masks
        # create dataset no water present: water (0) and no water (1) (128 clear and wet; 132 clear and wet and sea)
        # invert used here otherwise would be water (1) and no water (0)
        no_water = np.invert( (data['water'] == 128) | (data['water'] == 132) )
        
        # where uncertainty error of pixel is high (i.e. > 30) and no water is present (1)
        # this is to remove bad terrestrial data that we not picked up by the water mask of the vp
        high_ue = (data['UE'] > 30) & no_water
        
        # Set to null areas with high UE, but no water
        data_nan = data_nan.where(np.invert(high_ue))
        
        # Don't need this anymore, done its job
        data_nan = data_nan.drop_vars("UE")
        del high_ue

        # Where there's water, set PV to 0
        data_nan['PV'] = data_nan.PV.where(no_water, 0)
        data_nan['NPV'] = data_nan.NPV.where(no_water, 0)

        # Where there's water, set BS to 100
        data_nan['BS'] = data_nan.BS.where(no_water, 0)

        # Don't need this anymore
        data_nan = data_nan.drop_vars("water")
        del no_water

        # Resample to combine each 2 months of data into a median composite
        data_bimonthly = data_nan.resample(time='2m', closed='left').median()
        
        # Calculate summary bimonthly metrics
        minPV = data_bimonthly.PV.min(dim='time')
        maxPV = data_bimonthly.PV.max(dim='time')
        minNPV = data_bimonthly.NPV.min(dim='time')
        maxNPV = data_bimonthly.NPV.max(dim='time')
        minBS = data_bimonthly.BS.min(dim='time')
        maxBS = data_bimonthly.BS.max(dim='time')

        ### bimonthly max
        # Create mask where PV > BS
        PV_dominant = maxPV > maxBS
        # also have clause that PVmax must be > 50 meaning that the equivilant BS value for PVmax had to have been the dominant endmember
        # I think in effect this will mean non veg had to have been very evident for the year
        PV_dominant_PV50 = maxPV > 50
        PV_dominant_total = PV_dominant + PV_dominant_PV50
        PV_dominant_nan = PV_dominant_total.where((maxPV >= 0) & (maxBS >= 0))

        # Create mask where NPV > BS
        NPV_dominant = maxNPV > maxBS
        NPV_dominant_NPV50 = maxNPV > 50
        NPV_dominant_total = NPV_dominant + NPV_dominant_NPV50
        NPV_dominant_nan = NPV_dominant_total.where((maxNPV >= 0) & (maxBS >= 0))
        
        # Veg where PV or NPV are greater than BS
        tv_mask = (PV_dominant_nan + NPV_dominant_nan)

        # Make values of 2 == 1 (end result in binary veg mask)
        tv_mask_drop = np.where(tv_mask == 2, 1, tv_mask)
        tv_maxbimonthly_mask_xr = xr.DataArray(tv_mask_drop, coords=tv_mask.coords, dims=tv_mask.dims)

        ### bimonthly min
        # Create mask where PV > BS
        PV_dominant = minPV > minBS
        PV_dominant_PV50 = minPV > 50
        PV_dominant_total = PV_dominant + PV_dominant_PV50
        PV_dominant_nan = PV_dominant_total.where((minPV >= 0) & (minBS >= 0))

        # Create mask where NPV > BS
        NPV_dominant = minNPV > minBS
        NPV_dominant_NPV50 = minNPV > 50
        NPV_dominant_total = NPV_dominant + NPV_dominant_NPV50
        NPV_dominant_nan = NPV_dominant_total.where((minNPV >= 0) & (minBS >= 0))
        
        # Veg where PV or NPV are greater than BS
        tv_mask = (PV_dominant_nan + NPV_dominant_nan)

        # Make values of 2 == 1 (end result in binary veg mask)
        tv_mask_drop = np.where(tv_mask == 2, 1, tv_mask)
        tv_minbimonthly_mask_xr = xr.DataArray(tv_mask_drop, coords=tv_mask.coords, dims=tv_mask.dims)
        
        # combining max and min (i.e. veg == where tv_maxbimonthly_mask_xr and tv_minbimonthly_mask_xr say veg)
        tv_mask_combined = (tv_maxbimonthly_mask_xr + tv_minbimonthly_mask_xr)
        
        # tv_mask_drop_combined_or == max or min veg must be satisfied (i.e. more lenient veg mask)
        tv_mask_drop_combined_or = np.where(tv_mask_combined == 2, 1, tv_mask_combined)
        tv_bimonthly_combined_or = xr.DataArray(tv_mask_drop_combined_or, coords=tv_mask_combined.coords, dims=tv_mask_combined.dims)
        
        return tv_bimonthly_combined_or.to_dataset(name='fc_bimonthly_50')

    def measurements(self, input_measurements):
        return {'fc_bimonthly_50': Measurement(name='fc_bimonthly_50', dtype='float32', nodata=float('nan'), units='1')}        
        

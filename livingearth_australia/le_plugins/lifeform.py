from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy as np

class lifeform(Transformation):
    '''
    Use the woody cover fraction to define Woody (1) and Herbaceous (2)
    '''
    def compute(self, data):
        woody = data.woody_cover
        woody.values = np.where(woody >= 0.2, 1, 2)
        return woody.to_dataset(name='lifeform_veg_cat')

    def measurements(self, input_measurements):
        return {'lifeform_veg_cat': Measurement(name='lifeform_veg_cat', dtype='float32', nodata=float('nan'), units='1')}
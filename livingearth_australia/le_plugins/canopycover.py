from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy
import copy
from datacube.storage import masking

class canopycover(Transformation):
    def compute(self, data):
        
        masked_fc = masking.mask_invalid_data(data)
        masked_fc['pv'] = masked_fc.pv.where(masked_fc.ue < 30)
        
        fc_med = masked_fc.pv.median(axis=0)
        # vegetation by defintion must have canopycover of >0 %, therefore if PV values are < 1, assume they are 1%
        # and categoried as scattered 1-4% canopycover
        canopycover = numpy.where(((fc_med <= 100) & (fc_med >= 1)), fc_med, 1)
        out = xr.Dataset({'canopyco_veg_con': (data.pv[0].dims, canopycover)}, coords=fc_med.coords, attrs=fc_med.attrs)

        return out

    def measurements(self, input_measurements):
        return {'canopyco_veg_con': Measurement(name='canopyco_veg_con', dtype='float32', nodata=float('nan'), units='1')}

from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy as np

class wcf_mask(Transformation):
    '''
    Create a vegetated area mask by thresholding out very low Woody Cover Fraction values.
    '''

    def compute(self, data):
        woody = data.woody_cover
        woody.values = np.where(woody >= 0.004, 1, 0)
        return woody.to_dataset(name='wcf_mask')

    def measurements(self, input_measurements):
        return {'wcf_mask': Measurement(name='wcf_mask', dtype='float32', nodata=float('nan'), units='1')}
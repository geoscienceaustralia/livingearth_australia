from datacube.virtual import construct, Transformation, Measurement
import xarray as xr
import numpy as np
import copy
from itertools import groupby
from datacube.storage import masking
from datacube.testutils.io import rio_slurp_xarray
import datacube

class best_pixel_mad(Transformation):
    '''
    Get best available data for annual Landsat products
    Products must be provided in the following order:
                
    collate:
        - product: ls8_nbart_tmad_annual
        - product: ls5_nbart_tmad_annual
        - product: ls7_nbart_tmad_annual
    '''

    def compute(self, data):
        if 0 in data.sensor.values:
            # Landsat 8
            data = data.where(data.sensor==0, drop=True)
        elif 1 in data.sensor.values:
            # Landsat 5
            data = data.where(data.sensor==1, drop=True)
        elif 2 in data.sensor.values:
            # Landsat 7
            data = data.where(data.sensor==2, drop=True)
        return data.drop(['sensor', 'time']).squeeze()

    def measurements(self, input_measurements):
        return {'best_pixel_mad': Measurement(name='best_pixel_mad', dtype='float32', nodata=float('nan'), units='1')}

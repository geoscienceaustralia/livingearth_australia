import boto3
import os
import numpy as np
import argparse
import datetime

SQS_QUEUE = os.environ.get("SQS_QUEUE", 'dea-dev-eks-lccs')

sqs = boto3.resource('sqs')
queue = sqs.get_queue_by_name(QueueName=SQS_QUEUE)


def load_tiles(tile_file):
    with open(tile_file, 'r') as f:
        return [l.strip() for l in f]


def add_items(year):
    print(f"adding items for {year}")

    for tile in tiles:
        print(f"adding {tile}/{year}")
        queue.send_message(MessageBody=tile+"/"+year)


if __name__ == "__main__":
    print("Starting to add to queue")

    parser=argparse.ArgumentParser(description="Run LE LCCS Classification for selection of tiles",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--year', nargs='+', required=True, help="Year(s) of analysis (i.e. 2015 2016)")
    parser.add_argument('--tiles', required=True, help="Text file containing Albers Tiles to be processed")

    args = parser.parse_args()

    tiles=load_tiles(args.tiles)

    current_year = datetime.datetime.now().year

    if any(int(i) > current_year for i in args.year) or any(int(i) < 1987 for i in args.year):
            raise ValueError(
                    "Provided years are out of range for available data"
            )

    if len(args.year) ==2:
        years = np.arange(int(args.year[0]), int(args.year[1]) + 1)
        for year in years:
                add_items(str(year))
    else:
        for year in args.year:
                add_items(year)

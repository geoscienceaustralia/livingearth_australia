# Create a text file containing a list of tiles for processing
import numpy as np
a = np.array([np.repeat(-13, 16), np.arange(-39,-23)])
b = np.array([np.repeat(-14, 16), np.arange(-39,-23)])
c = np.array([np.repeat(-15, 16), np.arange(-39,-23)])

out = np.concatenate([a, b, c], axis=1) 
np.savetxt("tilestrip_west.txt", out.astype(int).transpose(), fmt='%i', delimiter=" ")

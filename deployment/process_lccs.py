import os
import sys
import boto3
import shutil
import yaml
import logging
import glob
import warnings
import numpy
import click
import datetime
import pandas as pd
from pathlib import Path
from dateutil import relativedelta

from le_lccs.le_utils.gridded_classification import run_classification
from concurrent.futures import ThreadPoolExecutor, as_completed

_log = logging.getLogger(__name__)

SQS_QUEUE = os.environ.get("SQS_QUEUE", "dea-dev-eks-lccs")
S3_BUCKET = os.environ.get("S3_BUCKET", "dea-public-data-dev")
S3_PATH = os.environ.get("S3_PATH", "")
BASEPATH = os.environ.get("BASEPATH", "/tmp/lccs")
SESSION_DURATION = os.getenv("SESSION_DURATION", 43000)  # Default to ~11hours

VISIBILITYTIMEOUT = os.environ.get("VISIBILITYTIMEOUT", 3600)
os.environ["NUMEXPR_MAX_THREADS"] = "16"

# Set up boto3 connections for sqs and s3 resources
sqs = boto3.resource("sqs")
queue = sqs.get_queue_by_name(QueueName=SQS_QUEUE)
s3c=boto3.client('s3')

def setup_logging(level: int = -1):
    """
    Setup logging to print to stdout with default logging level being INFO.
    """

    if level < 0:
        level = logging.INFO

    logging.basicConfig(
        level=level,
        format="[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s",
        stream=sys.stdout,
    )

    # Disable warnings from Numpy and tensorflow, theses warnings are
    # from the model files hence they're silenced in the runner
    # TODO
    # All the warnings must be fixed in the source and following code
    #  must be removed in the future.
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # FATAL
    logging.getLogger("tensorflow").setLevel(logging.FATAL)
    warnings.simplefilter("ignore", category=DeprecationWarning)
    numpy.warnings.filterwarnings("ignore")


def load_config(conf):
    with open(conf, "r") as f:
        return yaml.safe_load(f)


def get_tile_bounds(tile):
    """
    Takes the six chatacter tile name ie: 'x01y01' for a tile in the collection
    3 Albers tile grid and returns the coordinates in easting/northing
    for EPSG:3577
    """

    x_value, y_value = tile.split("y")
    x_value = int(x_value[1:3])
    y_value = int(y_value)

    minx = -2688000 + (96000 * x_value)
    maxx = minx + 96000
    miny = -5472000 + (96000 * y_value)
    maxy = miny + 96000

    return {"minx": minx, "miny": miny, "maxx": maxx, "maxy": maxy}


def override_config(conf, tile, bounds, year, run_type, outpath=""):
    # Tile of the format: {'minx':minx,  'miny':miny, 'maxx':maxx, 'maxy':maxy}
    conf["extent"]["min_x"] = bounds["minx"]
    conf["extent"]["min_y"] = bounds["miny"]
    conf["extent"]["max_x"] = bounds["maxx"]
    conf["extent"]["max_y"] = bounds["maxy"]

    # update start and end times
    if run_type == "cyear":
        start = f"{year}-01-01"
        end = f"{year}-12-31"
    else:
        start = f"{year}-07-01"
        end = f"{int(year)+1}-06-30"


    basefile_path = f"{conf.get('product_name')}_{tile}_{start}"
    conf["Outputs"]["level3_data"]["output_file"] = (
        os.path.join(outpath, basefile_path) + "_L3_v-0.5.0.tif"
    )  # TODO add auto tagging for version here.
    conf["Outputs"]["level3_rgb"]["output_file"] = (
        os.path.join(outpath, basefile_path) + "_L3_rgb_v-0.5.0.tif"
    )

    conf["Outputs"]["level4_data"]["output_file"] = (
        os.path.join(outpath, basefile_path) + "_L4_v-0.5.0.tif"
    )
    conf["Outputs"]["level4_rgb"]["output_file"] = (
        os.path.join(outpath, basefile_path) + "_L4_rgb_v-0.5.0.tif"
    )

    # list of layers which need start & end dates
    l3_layers = [
        "vegetat_veg_cat",
        "wo_mask",
        "mangrove",
        "sklearn_cultivated_classification",
        "tf_urban_classification",
        "wcf_mask",
    ]

    for layer in l3_layers:
        conf["L3layers"][layer]["start_time"] = start
        conf["L3layers"][layer]["end_time"] = end

    l4_layers = [
        "lifeform_veg_cat",
        "canopyco_veg_con",
        "watersea_veg_cat",
        "waterper_wat_cin",
        "baregrad_phy_con",
    ]

    for layer in l4_layers:
        conf["L4layers"][layer]["start_time"] = start
        conf["L4layers"][layer]["end_time"] = end

    yaml_name = basefile_path + ".yaml"
    return yaml_name, conf


def save_yaml(filename, conf, outpath=""):
    with open(outpath + filename, "w") as outfile:
        outfile.write(yaml.dump(conf))
        outfile.flush()
        outfile.close()


def upload_to_s3(path, files, tile_id=None):
    def write_object(out_file):
        data = open(out_file, "rb")
        key = "{}/{}/{}".format(S3_PATH, path, os.path.basename(out_file))
        s3c.put_object(Bucket=S3_BUCKET, Key=key, Body=data)
        os.remove(out_file)

    if S3_BUCKET:
        logging.info("Uploading to {}".format(S3_BUCKET))

        uploaded_files = []
        max_num_threads = 5
        with ThreadPoolExecutor(max_workers=max_num_threads) as executor:
            task = {
                executor.submit(write_object, out_file): out_file
                for out_file in files
            }
            for future in as_completed(task):
                file_to_upload = task[future]
                try:
                    result = future.result()
                except Exception as exc:
                    raise ValueError(f"{file_to_upload} failed to copy")
                else:
                    uploaded_files.append(result)
    else:
        logging.warning("Not uploading to S3, because the bucket isn't set.")
    if tile_id is not None:
        logging.info(
            f"tile {tile_id}: uploaded {len(uploaded_files)} files to {S3_BUCKET}"
        )
    else:
        logging.info(f"Uploading profiling results to {S3_BUCKET}/{path}")


def count_messages():
    queue.load()
    return int(queue.attributes["ApproximateNumberOfMessages"])


def save_profiling_results():
    if Path(BASEPATH).exists():
        shutil.rmtree(BASEPATH)
    os.makedirs(BASEPATH)
    profiling = pd.concat(profiling_results)
    profiling.to_csv(f"{BASEPATH}/profiling.csv")
    ax = profiling.mean().plot(kind="bar", title="Mean Runtime")
    ax.set_xlabel("Layer")
    ax.set_ylabel("Time (seconds)")
    ax.get_figure().savefig(f"{BASEPATH}/mean_runtime.png")
    files = glob.glob(f"{BASEPATH}/**/*.*", recursive=True)
    path = "profiling/" + datetime.today().isoformat()
    upload_to_s3(path, files)


@click.command()
@click.option(
    "--profile", is_flag=True, help="Use this option to enable profiling"
)
@click.option(
    "--run-type",
    help="Use this option to run calendar or financial years, options are cyear and fyear",
)
def process_tiles(profile, run_type):
    setup_logging()

    if run_type not in ["cyear", "fyear"]:
        raise ValueError(
            "Run-type must be cyear for calendar years or fyear for financial years"
        )

    lccs_config = (
        "/opt/livingearth_australia/livingearth_australia/templates/l4_vp_template.yaml"
        if run_type == "cyear"
        else "/opt/livingearth_australia/livingearth_australia/templates/l4_vp_fy_template.yaml"
    )

    _log.info(
        {
            "SQS": SQS_QUEUE,
            "Bucket": S3_BUCKET,
            "Path": S3_PATH,
            "LCCS Config": lccs_config,
        }
    )
    n_messages = count_messages()
    conf_template = load_config(lccs_config)
    profiling_results = []
    while n_messages > 0:
        messages = queue.receive_messages(
            VisibilityTimeout=VISIBILITYTIMEOUT, MaxNumberOfMessages=1
        )

        if len(messages) > 0:
            message = messages[0]

            # break message into tile / year
            tile, year = message.body.split("/")
            _log.info(f"Processing {tile} for {year}")
            bounds = get_tile_bounds(tile)

            if Path(BASEPATH).exists():
                shutil.rmtree(BASEPATH)
            os.makedirs(BASEPATH)

            # Update date ranges in level-3 and level-4.
            # Also update the extent and level-3 and level-4 rgb output filenames.
            yaml_name, conf = override_config(
                conf_template, tile, bounds, year, run_type, BASEPATH
            )
            save_yaml(yaml_name, conf)
            start_date = datetime.datetime.strptime(year, "%Y")
            if run_type == "fyear":
                start_date = start_date  + relativedelta.relativedelta(months=6)
            product_date = "{:%Y-%m-%d}".format(start_date)
            # run lccs code
            profiling_results.append(
                run_classification(
                    config_file=yaml_name,
                    product_date=product_date,
                    profiling_flag=profile,
                    tile_id=tile
                )
            )

            files = glob.glob(f"{BASEPATH}/**/*.*", recursive=True)
            # upload to S3
            x, y = tile.split("y")
            path = f"{year}/{x}/y{y}"
            upload_to_s3(path, files, tile)

            os.remove(yaml_name)
            message.delete()
        else:
            _log.info(f"No messages found, queue: {SQS_QUEUE} is empty")

        n_messages = count_messages()

    # Save profiling results to S3-BUCKET/S3-PATH/profiling path
    if profile:
        save_profiling_results(profiling_results)


if __name__ == "__main__":
    process_tiles()
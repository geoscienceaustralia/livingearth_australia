# Running LCCS level 3 within DEA


### Getting Started
##### Repositories
You will need both [LivingEarth LCCS](https://bitbucket.org/au-eoed/livingearth_lccs/src/master/) and [LivingEarth Australia](https://bitbucket.org/au-eoed/livingearth_australia/src/master/) repositories
``` git clone git@bitbucket.org:au-eoed/livingearth_lccs.git```
``` git clone git@bitbucket.org/au-eoed/livingearth_australia.git```

Within the LivingEarth LCCS repo run
```python setup.py install --user```
Ensure `PYTHONPATH` includes `livingearth_lccs`

##### LCCS development cube
Ensure you are pointing to the `LCCS_dev` datacube environment as this contains the MADs and mangrove layers required for level 3 classification

1. Alter your datacube config file
Locate your `datacube.conf` file in the home directory. It is suggested you make a copy of the original.
Append to include `[lccs_dev]`
2. Reference the new config file
`DATACUBE_CONFIG_PATH="path/to/your/new/file/datacube.conf"`
3. Change to lccs development datacube
`datacube -E lccs_dev`
NOTE: For jupyter notebooks use
`dc = datacube.Datacube(env='lccs_dev')`

### Running LCCS level 3

1. Navigate to LivingEarth Australia repo
`livingearth_australia/template`
This contains a level 3 template `l3_le_template.yaml` as well as a script to populate the template `pop_l3_le_template.py` with the 16 selected test sites from a [.gpkg](https://bitbucket.org/au-eoed/livingearth_australia/src/master/vec_rois/level3_test_sites.gpkg)
2. Populate the template
```python pop_l3_le_template.py -f ../vec_rois/level3_test_sites.gpkg -l level3_test_sites -t ./l3_le_template.yaml -o ./out_configs/ -c level3.sh -p ../imgouts/```
This will generate an executable shell script `level.sh`
3. Run shell script
`bash level3.sh`
output images will be saved in folder `../imgouts/`

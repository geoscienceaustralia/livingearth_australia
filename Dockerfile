FROM mambaorg/micromamba:git-7545124-jammy as lccs-conda

USER root
COPY env.yaml /conf/
RUN micromamba create  -y -p /env -f /conf/env.yaml && \
    micromamba clean --all --yes && \
    micromamba env export -p /env --explicit

ARG MAMBA_DOCKERFILE_ACTIVATE=1
COPY requirements.txt /conf/
RUN micromamba run -p /env pip3 install --no-cache-dir \
    --no-build-isolation -r /conf/requirements.txt

FROM ubuntu:jammy-20220815

COPY --from=lccs-conda /env /env
ENV GDAL_DRIVER_PATH=/env/lib/gdalplugins \
    PROJ_LIB=/env/share/proj \
    GDAL_DATA=/env/share/gdal \
    PATH=/env/bin:$PATH

WORKDIR /opt/

COPY . livingearth_australia
RUN cd livingearth_australia &&  git submodule update --init
RUN cd livingearth_australia/livingearth_lccs && python -m pip install .

# Copy main apps to the working directory
RUN mv livingearth_australia/deployment/* .

ENV PYTHONPATH=${py_env_path}:'/opt/livingearth_australia/livingearth_australia/'
ENV AWS_NO_SIGN_REQUEST=Y
